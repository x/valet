.. -*- rst -*-

======
Groups
======

List groups
===========

.. rest_method:: GET /v1/groups

Lists groups.

Response Codes
--------------

.. rest_status_code:: success ../status.yaml


.. rest_status_code:: error ../status.yaml


Request Parameters
------------------

.. rest_parameters:: parameters.yaml



Response Parameters
-------------------

.. rest_parameters:: parameters.yaml


Response Example
----------------

.. literalinclude:: ./samples/something.json
   :language: javascript
