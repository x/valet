#!/usr/bin/env bash

#Python Package installation in venv
#===================================
pip install daemon
pip install pyparsing
pip install requests
pip install python-daemon
pip install simplejson
pip install pika
pip install python-keystoneclient
pip install python-novaclient==6.0.0
pip install python-heatclient
pip install oslo.db
pip install oslo.messaging
pip install pecan==1.1.1
pip install notario==0.0.11
pip install pecan-notario==0.0.3

