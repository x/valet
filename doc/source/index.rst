Welcome to Valet's documentation!
=================================

.. toctree::
   :maxdepth: 1

   contributing
   heat

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
