#
# Copyright 2014-2017 AT&T Intellectual Property
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import uuid

from valet.api.db.models.music import groups


def group(name="mock_group", description="mock group", type="affinity",
          level="host", members='["test_tenant_id"]'):
    """Boilerplate for creating a group"""
    group = groups.Group(name=name, description=description, type=type,
                         level=level, members=members, _insert=False)
    group.id = str(uuid.uuid4())
    return group
